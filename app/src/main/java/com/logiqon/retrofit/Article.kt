package com.logiqon.retrofit


data class Article(
    var auther: String,
    var title: String,
    var description: String,
    var url: String,
    var urlToImage: String,
)