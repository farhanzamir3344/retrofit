package com.logiqon.retrofit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.logiqon.retrofit.databinding.ActivityMainBinding
import retrofit2.Call
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    lateinit var adapter: NewsAdapter
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        getNews()

    }

    private fun getNews() {
        val news = NewsService.newsInstance.getHeadlines("us", 1)
        news.enqueue(object : retrofit2.Callback<News> {

            override fun onFailure(call: Call<News>, t: Throwable) {
                Log.d("apiNews", "Error in Fetching News", t)

            }

            override fun onResponse(call: Call<News>, response: Response<News>) {

                val newsHeadlines = response.body()
                if (newsHeadlines != null) {
                    Log.d("apiNews", newsHeadlines.toString())
                    adapter = NewsAdapter(this@MainActivity, newsHeadlines.articles)
                    binding.recyclerview.adapter = adapter
                    binding.recyclerview.layoutManager = GridLayoutManager(this@MainActivity, 3)

                }
            }
        })
    }
}
