package com.logiqon.retrofit

data class News(val totalResults: Int, val articles: List<Article>)